# Overview

An effort was started under DEV-950 to standardize Python practices for Python developers at Baxter. This repo represents the effort in DEV-943 (i.e., a proof of concept for unit testing). The goal of this document is to lay the groundwork for the tools we use, the basics of the tools, and a baseline for how we build tests.

## Info
* This is not intended to be an exhaustive "how-to" but rather an introduction to topics and concepts we will utilize in the MaaS team for testing.
* You have to run `app.py` as a module `python -m src.app`
* Install the environment for this repo using `conda-lock install --name YOURENV conda-lock.yml`

# Topics
There is an order for understanding some of our practices. A prerequisite to this repo is understanding how we manage environment. If you have not read up on that, please do that first.

__Table of Contents__
* [Tools](#test-specific-tools)
* [Project Structure](#project-structure)
* [Fundamentals](#fundamentals)
* [Test Driven Development](#test-driven-development)
* [Resources](#resources)

## Test Specific Tools
| tool |  description|
|--|--|
| [pytest](https://docs.pytest.org/) | Our core tool for managing unit testing in Python is just about the industry standard |
| [coverage.py via pytest-cov](https://pytest-cov.readthedocs.io/)| An integration tool to expose what code is covered by pytests and by how much

## Project Structure
Below are two different examples of a project structure. There are a few main points to make here:
1. The `tests` directory should sit in the same space as your `src` directory (aka, `src` or `<project_name>` directory).
2. There are different kinds of tests—a topic that is too deep to spell out here in detail—but you can refer to this [resource](#different-test-types). These different types should be explicit beneath the `tests` directory and broken up into at least `unit` and `integration`. 
3. Beneath `unit`, the file structure should reflect that of `src` to the best of your ability.
4. The `conftest.py` file should be the file that holds your project's pytest fixtures.

A few notes about these examples:
* I purposely left out all `__init__.py` files to reduce clutter; you don't have to sift through them here, but your project will require them.
* The `.` represents the root of a project's Git repository.
* Remember that there are many tests inside a `test_*.py` file. In the examples, the `test_*.py` files represent chunks of the project which will have many smaller tests inside (i.e., `def test_ASpecficPart`).

### Example A: Simple Project
```bash
.
└── src
    ├── modules
    │   ├── module_A.py
    │   └── module_B.py
    ├── tests
    │   ├── conftest.py
    │   ├── unit
    │   │   └── modules
    │   │       ├── test_module_A_logic.py
    │   │       └── test_module_B_logic.py
    │   └── integration
    │       └── test_app.py
    └── app.py
 # ... other root files
```
### Example B: Flask RESTful API Project
```bash
.
├── src
│   ├── api
│   │   └── resources
│   │       ├── endpoint_A.py
│   │       └── ...
│   ├── commons
│   │   ├── functions.py
│   │   └── classes.py
│   ├── db
│   │   └── connect.py
│   ├── config.py
│   └── tests
│       ├── conftest.py
│       ├── unit
│       │   ├── api
│       │   │   └── resources
│       │   │       ├── test_endpoint_A_logic.py
│       │   │       │   └── ...
│       │   │       └── ...
│       │   └── db
│       │       └── test_database_logic.py
│       └── integration
│           ├── test_db_connection.py
│           └── test_endpoint_healthcheck.py
└── environment.yml
```


## Fundamentals
`pytest` is run in the shell and has a built-in "discovery" functionality that looks for files (and objects therein) with names like `test_` or `_test`. Once you have a basic test, you can run the following in your shell terminal:
```bash
$ pytest
```
This command will invoke the "discovery" functionality based on your working directory and run any tests found.
### Testing Objects

Test files tell `pytest` what files to test while test objects tell `pytest` what objects to test.

#### Examples

##### Functions

Test a function `return_zero`:
```python
# ./test_file.py
def return_zero():
	return 0


def test_this():
	assert return_zero() == 0
```
##### Classes
Test a class `Mug`:
```python
# ./src/classes/mug.py
class Mug:
	def __init__(self, contents='coffee'):
		self.contents = contents


# ./src/tests/classes/tes_mug.py
from src.classes.mug import Mug


def test_coffee_mug():
	coffee_mug = Mug()
	assert coffee_mug.contents == 'coffee'

def test_tea_mug():
	tea_mug = Mug('tea')
	assert coffee_mug.contents == 'tea'
```
A convenience of `pytest` is its "fixture" decorator `@pytest.fixture` which can be used to reduce code clutter for testing classes. Fixtures are automatically injected into the scope of the tests. For example:
```python
# ./src/classes/mug.py
class Mug:
	def __init__(self, contents='coffee'):
		self.contents = contents


# ./src/tests/conftest.py
import pytest
from mug import Mug


@pytest.fixture
def coffee_mug():
	"""Returns a mug containing 'coffee'."""
	return Mug()


@pytest.fixture
def tea_mug():
	"""Returns a mug containing 'coffee'."""
	return Mug('tea')


#./src/tests/classes/test_mug.py
# Note: No import of fixtures required here.
def test_coffee_mug(coffee_mug):
	assert coffee_mug.contents == 'coffee'


def test_tea_mug(tea_mug):
	assert tea_mug.contents == 'tea'
```

## Run Tests
Run a test pointed at your `./src/tests` directory:
```bash
$ pytest "src/tests"
```
Run a test with coverage:
```bash
$ pytest "src/tests" --cov="src"
```

## Test Driven Development
There is a write-tests-first philosophy is known as Test Driven Development. This philosophy for development heavily utilizes unit testing as a means to develop robust code. While this is not a requirement for how you develop your code as a MaaS Python Developer, it is certainly good to be familiar with this practice and to utilize some of the concepts therein. You can watch the [training video](#tdd-training-video) in resources below to get a better idea for the practical src of TDD.

## Baxter Python Unit Test Development
__Every ticket requires tests__. Any development that touches code that will eventually end up in production must come with unit tests. This allows more robust development and includes team member feedback via PRs. However, it is not required your entire code base is covered with tests. For example, Google has the following [guideline](https://testing.googleblog.com/2020/08/code-coverage-best-practices.html):

>  Although there is no “ideal code coverage number,” at Google we offer the general guidelines of 60% as “acceptable”, 75% as “commendable” and 90% as “exemplary.”

__A place for everything—even your tests__. Follow the [project guidelines](#project-structure) above to better organize your tests so they are easy to find and manage in the scope of a Git workflow.

__Give it a name AND a docstring__. It is best practice to add some docstring to your tests and to give them explicit names. A convention for naming test functions breaks PEP8 conventional function naming and looks like, `def test_ThisMugContainsCoffee`. A little docstring can provide much needed context for developers down the road from you.

__Outline your code before you begin development__. When you are thinking of the work you will do on an assigned Jira ticket, don't just think through the functions and classes you might write, take the time to outline them and think of test cases.

__Write tests in parts__. Tenured developers at Baxter have noted that writing bulky tests will come back to bite you. The best way forward is to write small test cases to test individual parts of your functions and classes. It will take a little more time and be more tedious, but the promise is that it will be worth it later when it is easy to find what broke—even better, that you expected a test to fail. For example, you may have one module `./src/modules/maths.py` that has one test file `./src/tests/unit/modules/test_math.py` with multiple test functions inside.

__Use `pytest.fixtures` to reduce clutter__. Testing your classes will be less messy if you utilize a feature like `fixtures` in your testing. Remember, store fixtures in a central location `./src/tests/conftest.py`.

## Resources

#### TDD Training Video
A video outlining the basics of `pytest`, unit testing, and Test Driven Development.
TDD Training Video — [video](https://bybaxter-my.sharepoint.com/:v:/g/personal/jhartman_baxterplanning_com/Ec21CCpsnW5HrpmZwyany6IBsr-8Jre4BxvfxLAnpy9AuQ?e=pOU7HT)
#### Different Test Types
A stackoverflow explanation of the different kinds of tests.
Explanation — [link](https://stackoverflow.com/questions/2741832/what-is-the-difference-between-unit-tests-and-functional-tests/37564654#37564654)
#### Testing NumPy or Pandas
A video explaining how to structure tests for NumPy and Pandas.
Video — [video](https://youtu.be/ovvU41ve1FQ)