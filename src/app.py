from .classes.mug import Mug
from .modules import increment

coffee_mug = Mug("coffee")
number_of_coffees = increment.by_one(5)
