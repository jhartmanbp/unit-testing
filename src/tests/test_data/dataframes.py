import pandas as pd


x = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
y = [num**2 for num in x]

data = dict(x=x, y=y)

dataframe = pd.DataFrame(data)

if __name__ == '__main__':
    print(dataframe)
