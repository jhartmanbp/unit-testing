from src.classes.mug import Mug


def test_contains_coffee():
    mug = Mug("coffee")
    assert mug.contains == "coffee"


def test_fixture_mug_contains_tea(tea_mug):
    assert tea_mug.contains == "tea"
