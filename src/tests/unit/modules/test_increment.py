import pytest

from src.modules import increment


def test_incremement():
    assert increment.by_one(1) == 2


def test_dataframe_increment(dataframe_one):
    dataframe_one['x'].loc[0] = increment.by_one(dataframe_one['x'].loc[0])
    assert dataframe_one['x'].loc[0] == 2


def test_dataframe_increment_fail(dataframe_one):
    with pytest.raises(AssertionError):
        dataframe_one['x'].loc[0] = increment.by_one(dataframe_one['x'].loc[0])
        assert dataframe_one['x'].loc[0] == 3
