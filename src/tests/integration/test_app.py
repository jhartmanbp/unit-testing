from src.app import coffee_mug, number_of_coffees


def test_coffee_mug():
    assert coffee_mug.contains == "coffee"


def test_number_of_coffees():
    assert number_of_coffees == 6
