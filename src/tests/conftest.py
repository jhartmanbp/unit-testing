import pytest

from src.classes.mug import Mug
from src.tests.test_data.dataframes import dataframe


@pytest.fixture
def coffee_mug():
    return Mug("coffee")


@pytest.fixture
def tea_mug():
    return Mug("tea")


@pytest.fixture(scope='function')
def dataframe_one():
    return dataframe.copy()
